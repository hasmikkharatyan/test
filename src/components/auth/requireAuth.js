import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Route, Redirect} from 'react-router-dom';
import { useSelector } from 'react-redux'
//import auth0Client from '../Auth';
// import PropTypes from 'prop-types';

export default function (ComposedComponent) {
    class Authentication extends Component {
    // static contextTypes = {
    //     router: PropTypes.object,
    // };

    componentDidMount() {
        const {history, authenticated} = this.props;
        if (authenticated === null) {
           history.push('/');
        }
    }

    componentDidUpdate(nextProps) {
        const {history} = this.props;
        if (!nextProps.authenticated) {
            history.push('/');
        }
    }

    render() {
        const {authenticated} = this.props;
        if (authenticated) {
            return <ComposedComponent {...this.props} />;
        }
        return null;
    }
    }

    function mapStateToProps({auth}) {
        // const { details = {} } = state;
        return { authenticated: auth };
    }

    return connect(mapStateToProps)(Authentication);
}

export function SecuredRoute(props) {
  const {component: Component, path} = props;
  const authenticated = useSelector(state => state.auth)
  return (
    <Route exact path={path}>
      {!authenticated ? <Redirect to="/"  /> : <Component />}
    </Route>
  );
}

