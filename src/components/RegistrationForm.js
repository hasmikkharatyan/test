import React,{Component} from 'react'
// import '../style/css/test.css'
import { Field, reduxForm } from 'redux-form'
import DateTimePicker from 'react-widgets/lib/DateTimePicker'
import momentLocalizer from "react-widgets-moment";
import moment from 'moment'
import 'react-widgets/dist/css/react-widgets.css'
import validation from '../helper/validation';
import { connect } from 'react-redux';
import * as actions from '../actions';

momentLocalizer(moment)

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
      {/*<label>{label}</label>*/}
      <div className={(touched && error)?'error':''}>
        <input {...input} placeholder={label}  type={type}/>
          {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
)

const renderDateTimePicker = ({ input: { onChange, value }, showTime }) =>
    <DateTimePicker
        onChange={onChange}
        format="DD MMM YYYY"
        time={showTime}
        value={!value ? null : new Date(value)}
    />

class RegistrationForm extends Component{
    handleFormSubmit =  (data) => {
        const {birthDate} = data;
        data.birthDate = birthDate.toUTCString();
        // const { addFormValue } = this.state;
        const { registerUser, history } = this.props;
        // event.preventDefault();
        registerUser(data);
        history.push('/app');
        // this.setState({ addFormValue: '' });
    };
  render() {
      const {handleSubmit, pristine, reset, submitting} = this.props;
      const {required,email, password, matches} = validation;
      return (
          <form onSubmit={handleSubmit(this.handleFormSubmit)}>
            <Field name="firstName" type="text"
                   component={renderField} label="First name"
                   validate={[required]}
            />
            <Field name="lastName" type="text"
                   component={renderField} label="Last name"
                   validate={[required]}
            />
            <Field name="email" type="email"
                   component={renderField} label="Email"
                   validate={email}
            />
            <Field
                name="birthDate"
                showTime={false}
                component={renderDateTimePicker}
                time={false}
                validate={[required]}
            />
            <Field name="password" type="password"
                   component={renderField} label="Password"
                   validate={password}
            />
            <Field name="confirmPassword" type="password"
                   component={renderField} label="Confirm password"
                   validate={matches}
            />
            <div className="form-buttons">

              <button type="submit" className="round-button" disabled={submitting}>Sign up</button>
              <button type="button" className="round-button" disabled={pristine || submitting} onClick={reset}>Clear </button>
            </div>
          </form>
      )
  }
}

const mapStateToProps = ({ data, auth }) => {
    return {
        data,
        auth
    };
};

export default connect(mapStateToProps, actions)(reduxForm({
    form: 'fieldLevelValidation' // a unique identifier for this form
})(RegistrationForm))
// export default connect(mapStateToProps, actions)(List);

