import React, { Component } from 'react';
import { BrowserRouter, Route, NavLink, Switch} from 'react-router-dom';
import { connect } from 'react-redux';
import List from './components/List';
import SignIn from './components/SignIn';
import requireAuth,{SecuredRoute} from './components/auth/requireAuth';
import { fetchUser } from './actions';
import RegistrationForm from './components/RegistrationForm';
import UserEditForm from './components/UserEditForm';
import './style/App.scss';



class App extends Component {
    componentDidMount() {
        this.props.fetchUser();
    }

    render() {
      const {authenticated} = this.props;
        return (
            <BrowserRouter> 
              <div className="container">
              {!authenticated && (<ul class="tab-group">
                <li ClassName="tab">
                  <NavLink to="/register" activeClassName="active">Sign Up</NavLink></li>
                <li ClassName="tab">
                  <NavLink exact to="/" activeClassName="active">Log In</NavLink>
                </li>
              </ul>)}
              <Switch>
                <SecuredRoute exact path='/app' component={List} />
                <Route exact path="/" component={SignIn} />
                {/* <Route path="/app" component={requireAuth(List)} /> */}
                <Route exact path="/register" component={RegistrationForm} />
                <Route exact path="/edit/:id" component={requireAuth(UserEditForm)} />
                <SecuredRoute exact path='*' component={List} />
                </Switch>
              </div>
            </BrowserRouter>
        );
    }
}
function mapStateToProps({auth}) {
  return { authenticated: auth };
}

export default connect(mapStateToProps, { fetchUser })(App);