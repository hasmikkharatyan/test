import { databaseRef, authRef, provider, userRef } from '../config/firebase';

export const getUser = async(uid) => {
    const res = await databaseRef.ref('/users/' + uid).once('value');
    return res.val();
}