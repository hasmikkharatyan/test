import { SET_USER_INFO } from '../actions/types';

export default (state = 'loading', action) => {
    switch (action.type) {
        case SET_USER_INFO:
            return action.payload;
        default:
            return state;
    }
};
