import React from 'react';
import { mount } from 'enzyme';
import SignInForm, { renderField } from  '../components/signInForm'
import { shallow , configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockStore = configureStore([]);
configure({ adapter: new Adapter() });

const data = {email:'', password: ''};


describe('renderField', () => {
	let subject
		it("renders an error message for the input", () => {
		const input = { name: 'email', value: '' }
		const label = 'Email'
		const meta = { touched: true, error: 'Required' }
		const element = renderField({ input, label, meta })
		subject = shallow(element)
		const firstNameHelpBlock = subject.find('span').first()
		expect(firstNameHelpBlock.text()).toEqual('Required')
    })
    it("renders an error message for the input", () => {
		const input = { name: 'email', value: 'eee@ee' }
		const label = 'Email'
		const meta = { touched: true, error: 'Invalid email address' }
		const element = renderField({ input, label, meta })
		subject = shallow(element)
        const firstNameHelpBlock = subject.find('span')
		expect(firstNameHelpBlock.text()).toEqual('Invalid email address')
	})
})


describe('Login', () => {
  let store;
  let component;
  const mockLoginfn = jest.fn();
  beforeEach(() => {
    store = mockStore({
      myState: 'sample text',
    });
    
    store.dispatch = jest.fn();
     const fackFn =(param)=>{
       param();
     }
    component = mount(<Provider store={store}><SignInForm onHandleSubmit = {()=>fackFn(mockLoginfn)}/></Provider>);
  });

  it('should call the mock login function', () => {
      component.find('#loginForm').simulate(
        'submit', 
        {preventDefault() {}}
      )
      
      expect(mockLoginfn.mock.calls.length).toBe(1)
  })

  it('should update an input when it is changed', () => {
  
    
      component.find('input[type="email"]').simulate(
          'change', 
          [{target: 
            {name: 'email', value: 'blah@gmail.com'}
          }]
        )  
        component.find('input[type="password"]').simulate(
          'change', 
          [{target: 
            {name: 'password', value: '123456'}
          }]
        )  
        component.find('#loginForm').simulate(
          'submit', 
          {preventDefault() {}}
        )   
  });
})

