import React from 'react';
import { shallow, mount } from 'enzyme';
import Login from '../components/SignIn';
import SignInForm from  '../components/signInForm'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import { reducer } from 'redux-form';
import dataReducer from '../reducers/dataReducer';
import Thunk from 'redux-thunk';
import {fetchUserByUid, updateUserStore} from '../actions/index';
import * as servises from '../servises';

const middlewares = [Thunk] ;
const mockStore = configureStore(middlewares);

configure({ adapter: new Adapter() });
const data = {
    birthDate:"Sat, 11 Jan 2020 13:02:20 GMT",
    email:"dd@dd.dd",
    firstName:"ddd",
    lastName:"dd"
};
describe('Register', () => {

it("Reduser->sould update user info in state",()=>{
    const defoultState = [];
    
    const payload = data;
    const expected = data;
    const action = {
        type:'SET_USER_INFO',
        payload
    };
    expect(dataReducer(defoultState,action)).toEqual(expected);
});

it("Action->sould load user Info",()=>{
    servises.getUser = jest.fn().mockImplementation(()=>Promise.resolve(data));
    const dedoultState = [];
    const store = mockStore({
        data:{}
    });
    const expected = data;
    return store.dispatch(fetchUserByUid('testId')).then(()=>{
        const actions = store.getActions();
        expect(actions[0]).toEqual(updateUserStore(expected))
    });
});

});