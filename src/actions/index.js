import { databaseRef, authRef, userRef } from '../config/firebase';
import { getUser } from '../servises';
import { SET_USER_INFO, FETCH_USER } from './types';


export const registerUser = (newUser) => async (dispatch) => {
    const {email, password} = newUser;
    delete newUser.password;
    delete newUser.confirmPassword;
    const res = await authRef.createUserWithEmailAndPassword(email, password);
    await userRef.child(res.user.uid)
     .set(newUser);
};
export const editToDo = (newToDo, uid) => async (dispatch) => {
    userRef
         .child(uid)
        .set(newToDo);
    // dispatch({
    //     type:  FETCH_TODOS,
    //     payload: null,
    // });
};


export const completeToDo = (completeToDoId, uid) => async (dispatch) => {
    userRef
        .child(completeToDoId)
        .remove();
};

export const updateUserStore = payload => ({
    type: SET_USER_INFO,
    payload,
});

export const fetchUserByUid = uid => async (dispatch) => {
    const snapshot = await getUser(uid);
    dispatch(updateUserStore(snapshot));
};

export const fetchUser = () => (dispatch) => {
    authRef.onAuthStateChanged((user) => {
        if (user) {
            dispatch({
                type: FETCH_USER,
                payload: user,
            });
        } else {
            dispatch({
                type: FETCH_USER,
                payload: null,
            });
        }
    });
};

export const signIn = (email,pass) => (dispatch) => {
    authRef
        .signInWithEmailAndPassword(email,pass)
        .then((result) => {})
        .catch((error) => {
            console.log(error);
        });
};

export const signOut = () => (dispatch) => {
    authRef
        .signOut()
        .then(() => {
            // Sign-out successful.
        })
        .catch((error) => {
            console.log(error);
        });
};
