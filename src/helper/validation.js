
const required = value => value ? undefined : 'Required'
const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
        'Invalid email address' : undefined
const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength15 = maxLength(15)
const number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined
const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined
const minValue18 = minValue(18)

const password = value => value && !/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/i.test(value) ?
'Invalid password' : undefined


const matches = (val, vals) => (vals.password !== val ? 'not matches' : undefined);


export default  {required, email, password, matches}
